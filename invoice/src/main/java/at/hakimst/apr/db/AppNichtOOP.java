package at.hakimst.apr.db;

import java.math.BigDecimal;
import java.sql.*;
import java.util.Calendar;
import java.util.Scanner;

public class AppNichtOOP {
    public static void main(String[] args) {
        start();
    }

    public static void start() {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("1..zum Erstellen");
            System.out.println("2..zum Anzeigen aller Datensätze");
            System.out.println("3..zum Updaten");
            System.out.println("4..zum Löschen");
            System.out.println("5..zum Beenden");
            int i = scanner.nextInt();
            if (i == 1) {
                createInvoice();
            } else if (i == 2) {
                showInvoices();
            } else if (i == 3) {
                //TODO Update Methode aufrufen (davor neue Daten abfragen, welche Daten geändert werden)
            } else if (i == 4) {
                //TODO Delete Methode aufrufen (davor abfragen, welche ID gelöscht werden sollte)
            } else {
                System.out.println("Unbekannter Befehl");
                break;
            }


//            switch (i){
//                case 1: createInvoice();break;
//                case 2: showInvoices(); break;
//                //case 3: TODO
//                //case 4: TODO
//                default:
//                    System.out.println("Unbekannter Befehl");
//                    break;
//            }
        }

    }

    public static void createInvoice(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Geben Sie das Rechnungsdatum ein:");
        String date = scanner.nextLine();
        System.out.println("Beschreibung:");
        String descr = scanner.nextLine();
        System.out.println("Betrag:");
        double amount = scanner.nextDouble();
        System.out.println("Bereits bezahlt (true oder false)?");
        boolean paid = scanner.nextBoolean();
        insertInvoice(Date.valueOf(date), descr, amount, paid);
    }

    public static Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/rechnungsdb", "root", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return con;
    }

    public static void showInvoices() {
        String sql = "SELECT * FROM invoice";
        try {
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();

            while (set.next()) {
                String output = "";
                for (int i = 1; i <= numCols; i++) {
                    String column = set.getString(i);
                    output += i != numCols ? column + ", " : column;
                }
                System.out.println(output);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertInvoice(Date date, String description, double value, boolean paid) {
        try {
            String insertSql = "INSERT INTO invoice (date, description, value, paid) VALUES (?,?,?,?)";
            PreparedStatement ps = getConnection().prepareStatement(insertSql);
            ps.setDate(1, new java.sql.Date(date.getTime()));
            ps.setString(2, description);
            ps.setBigDecimal(3, BigDecimal.valueOf(value));
            ps.setBoolean(4, paid);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateInvoice(int id, Date date, String description, double value, boolean paid) {
        try {
            String insertSql = "UPDATE Invoice set date = ?, description = ?, value = ?, paid = ? WHERE ID = ?";
            PreparedStatement ps = getConnection().prepareStatement(insertSql);
            ps.setDate(1, new java.sql.Date(date.getTime()));
            ps.setString(2, description);
            ps.setBigDecimal(3, BigDecimal.valueOf(value));
            ps.setBoolean(4, paid);
            ps.setInt(5, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteInvoice(int id) {
        try {
            String insertSql = "DELETE FROM invoice where id = ?";
            PreparedStatement ps = getConnection().prepareStatement(insertSql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
