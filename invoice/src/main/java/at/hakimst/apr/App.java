package at.hakimst.apr;

import at.hakimst.apr.model.Invoice;
import at.hakimst.apr.strategy.CSVExportStrategy;
import at.hakimst.apr.strategy.Exporter;
import at.hakimst.apr.strategy.JSONExportStrategy;
import at.hakimst.apr.strategy.SQLExportStrategy;

import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

public class App {

    public static void main(String[] args) {
        List<Invoice> invoiceList = createDummyList(20000);
        Exporter exporter = new Exporter(new JSONExportStrategy());
        long start = System.currentTimeMillis();
        String export = exporter.executeExport(invoiceList);
        System.out.println(export);
        long dur = System.currentTimeMillis() - start;

        System.out.println("Duration in milliseconds: " + dur);
    }

    public static List<Invoice> createDummyList(int numElements) {
        List<Invoice> invoiceList = new ArrayList<Invoice>();
        for (int i = 1; i <= numElements; i++) {
            invoiceList.add(new Invoice(i, new Date(System.currentTimeMillis()), "PS" + i, Math.random() * i, false));
        }

        return invoiceList;
    }


}
