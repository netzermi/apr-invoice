package at.hakimst.apr.strategy;

import at.hakimst.apr.model.Invoice;

import java.util.List;

public class SQLExportStrategy implements IExportStrategy{
    private static final String TABLE_NAME = "invoices";
    private final static String SEP = ",";

    public String export(List<Invoice> invoiceList) {
        StringBuffer sql = new StringBuffer();
        for (Invoice invoice : invoiceList) {
            sql.append("INSERT INTO " + TABLE_NAME + " (id, date, description, value, paid) VALUES (" + invoice.getId() + SEP + invoice.getDate() + SEP + "\'" + invoice.getDescription() + "\'" + SEP + invoice.getValue() + SEP + invoice.isPaid()  + ")" +  System.getProperty("line.separator"));
        }
        return sql.toString();

     }
}
