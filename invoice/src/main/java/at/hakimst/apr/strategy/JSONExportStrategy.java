package at.hakimst.apr.strategy;

import at.hakimst.apr.model.Invoice;

import java.lang.reflect.Field;
import java.util.List;

public class JSONExportStrategy implements IExportStrategy{

    public String export(List<Invoice> invoiceList) {
        //TODO, z.B. [ {"Vorname":"Max", "Nachname":"Mustermann"}, {"Vorname":"Susi", "Nachname":"Sorglos"}]
        StringBuffer json = new StringBuffer();
        json.append("[\n");
        for(Invoice invoice : invoiceList){
            String seperator_end = invoiceList.lastIndexOf(invoice) == (invoiceList.size()-1) ? "":",";
            json.append("{\"id\":\""+invoice.getId() + ", \"date:\":\""+ invoice.getDate().toString() + "\", \"description\":\"" + invoice.getDescription() + "\", \"value\":\"" + invoice.getValue() + "\", \"isPaid\":\"" + invoice.isPaid() + "\"}" + seperator_end + "\n");
        }
        json.append("]");
        return json.toString();
    }
}
