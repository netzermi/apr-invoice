package at.hakimst.apr.db;

import at.hakimst.apr.model.Invoice;

import java.util.List;

/** Hier sind die sogenannten CRUD Methoden für die Klasse invoice
 * abstract definiert.
 * Diese Idee wird auch als ORM - objektrelationales Mapping bezeichnet
 */
public interface InvoiceDao {

    //C - eine Rechnung erzeugen
    public void addInvoice(Invoice invoice);
    //R - eine Rechnung auslesen bzw. alle Rechnngen auslesen
    public Invoice getInvoice(int id);
    public List<Invoice> getAllInvoices();
    //U - eine Rechnung aktualisieren
    public void updateInvoice(Invoice invoice);
    //D - eine Rechnung löschen
    public void deleteInvoice(Invoice invoice);
    public void deleteInvoice(int id);


}
