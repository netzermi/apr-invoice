package at.hakimst.apr.db;

import at.hakimst.apr.model.Invoice;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class InvoiceDaoImpl implements InvoiceDao{

    public void addInvoice(Invoice invoice) {
        try {
            String insertSql = "INSERT INTO invoice (date, description, value, paid) VALUES (?,?,?,?)";
            PreparedStatement ps = getConnection().prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);
            ps.setDate(1, new java.sql.Date(invoice.getDate().getTime()));
            ps.setString(2, invoice.getDescription());
            ps.setBigDecimal(3, BigDecimal.valueOf(invoice.getValue()));
            ps.setBoolean(4, invoice.isPaid());
            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            keys.next();
            invoice.setId(keys.getInt(1));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Invoice getInvoice(int id) {
        String sql = "SELECT * FROM invoice WHERE id=?";
        try {
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ps.setInt(1,id);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            set.next();
            Invoice invoice = new Invoice(id, set.getDate(2), set.getString(3), set.getDouble(4), set.getBoolean(5));
            return invoice;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Invoice> getAllInvoices() {
        List<Invoice> invoiceList = new ArrayList();
        String sql = "SELECT * FROM invoice";
        try {
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            while(set.next()){
                Invoice invoice = new Invoice(set.getInt(1), set.getDate(2), set.getString(3), set.getDouble(4), set.getBoolean(5));
                invoiceList.add(invoice);
            }

            return invoiceList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return invoiceList;
    }

    public void updateInvoice(Invoice invoice) {
        try {
            String insertSql = "UPDATE Invoice set date = ?, description = ?, value = ?, paid = ? WHERE ID = ?";
            PreparedStatement ps = getConnection().prepareStatement(insertSql);
            ps.setDate(1, new java.sql.Date(invoice.getDate().getTime()));
            ps.setString(2, invoice.getDescription());
            ps.setBigDecimal(3, BigDecimal.valueOf(invoice.getValue()));
            ps.setBoolean(4, invoice.isPaid());
            ps.setInt(5, invoice.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void deleteInvoice(Invoice invoice) {
        deleteInvoice(invoice.getId());
    }

    public void deleteInvoice(int id) {
        String sql = "DELETE FROM invoice WHERE id=?";
        try {
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ps.setInt(1,id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/rechnungsdb", "root", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return con;
    }
}
