package at.hakimst.apr.strategy;

import at.hakimst.apr.model.Invoice;

import java.sql.Date;
import java.util.List;

public class CSVExportStrategy implements IExportStrategy {
    private final static String SEPARATOR = ";";
    private final static boolean FAST_METHOD = true;


    public String export(List<Invoice> invoiceList) {
        if (!FAST_METHOD) {
            String csv = "id" + SEPARATOR + "date" + SEPARATOR + "description" + SEPARATOR + "value" + SEPARATOR + "paid" + System.getProperty("line.separator");
            for (Invoice invoice : invoiceList) {
                csv += invoice.getId() + SEPARATOR + invoice.getDate() + SEPARATOR + invoice.getDescription() + SEPARATOR + invoice.getValue() + SEPARATOR + invoice.isPaid() + System.getProperty("line.separator");
            }
            return csv;
        } else {
            StringBuffer csv = new StringBuffer("id" + SEPARATOR + "date" + SEPARATOR + "description" + SEPARATOR + "value" + SEPARATOR + "paid" + System.getProperty("line.separator"));
            for (Invoice invoice : invoiceList) {
                csv.append(invoice.getId() + SEPARATOR + invoice.getDate() + SEPARATOR + invoice.getDescription() + SEPARATOR + invoice.getValue() + SEPARATOR + invoice.isPaid() + System.getProperty("line.separator"));
            }
            return csv.toString();
        }
    }


}
