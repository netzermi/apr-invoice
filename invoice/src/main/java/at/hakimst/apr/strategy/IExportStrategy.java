package at.hakimst.apr.strategy;

import at.hakimst.apr.model.Invoice;

import java.util.List;

public interface IExportStrategy {

    public String export(List<Invoice> invoiceList);
}
