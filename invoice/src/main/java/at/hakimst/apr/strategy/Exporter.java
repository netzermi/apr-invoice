package at.hakimst.apr.strategy;

import at.hakimst.apr.model.Invoice;

import java.util.List;

public class Exporter {
    private IExportStrategy exportStrategy;

    public Exporter(IExportStrategy exportStrategy){
        this.exportStrategy = exportStrategy;
    }

    public String executeExport(List<Invoice> invoiceList){
        return exportStrategy.export(invoiceList);
    }
}
